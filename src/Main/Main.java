package Main;

import Principal.Metodos;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Scanner;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        Metodos met1 = new Metodos();
        try {
            Boolean bandera = false;
            while (bandera == false) {
                Scanner sc1 = new Scanner(System.in) ;
                String cmd = sc1.nextLine();
                String[] vector = cmd.split(" ");
                String primero;
                String segundo;
                String tercero;
                primero = vector[0];
                Path ruta = Paths.get(System.getProperty("user.dir"));
                switch (primero) {
                    case "help":
                        met1.help();
                        break;
                    case "cd":
                        met1.cd(vector, cmd);
                        break;
                    case "mkdir":
                        //Si paso mkdir 2 hola funciona 
                        if (vector.length == 1 || vector.length != 2) {
                            System.err.println("ERROR..formato incorrecto..[ mkdir nombreDirectorio ]");
                        } else {
                            segundo = vector[1];
                            met1.mkdir(segundo);
                        }
                        break;
                    case "info":
                        if (vector.length == 2) {
                            segundo = vector[1];
                            met1.info(segundo);
                        } else {
                            System.err.println("ERROR..formato incorrecto.. [info nombre]");
                        }
                        break;
                    case "cat":
                        if (vector.length == 2) {
                            segundo = vector[1];
                            met1.cat(segundo);
                        } else {
                            System.err.println("ERROR..formato incorrecto.. [cat nombreFichero]");
                        }
                        break;
                    case "top":
                        if (vector.length != 3) {
                            System.err.println("ERROR..formato incorrecto.. [top numeroLineas nombreFichero]");
                        } else {
                            segundo = vector[1];
                            tercero = vector[2];
                            met1.top(segundo, tercero);
                        }
                        break;
                    case "mkfile":
                        if (vector.length == 1) {
                            System.err.println("ERROR..El nombre del fichero está vacio..");
                        } else if (vector.length == 2) {
                            segundo = vector[1];
                            tercero = "";
                            met1.mkfile(segundo, tercero);
                        } else {
                            segundo = vector[1];
                            tercero = vector[2];
                            met1.mkfile(segundo, tercero);
                        }
                        break;
                    case "write":
                        if (vector.length == 3) {
                            segundo = vector[1];
                            tercero = vector[2];
                            if (segundo.isEmpty()) {
                                System.err.println("ERROR..El nombre del fichero está vacio..");
                            } else {
                                met1.write(segundo, tercero);
                            }
                        }else{
                            System.err.println("ERROR..formato incorrecto.. [write nombreFichero texto]");
                        }
                        break;
                    case "dir":
                        met1.dir(vector,cmd);
                        break;
                    case "close":
                        bandera = true;
                        met1.close();
                        break;
                    case "delete":
                        if (vector.length == 2) {
                            segundo = vector[1];
                            met1.delete(segundo);
                        } else {
                            System.err.println("ERROR..formato incorrecto [delete nombre]");
                        }
                        break;
                    case "clear":
                        met1.clear();
                        break;
                }
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }

}
