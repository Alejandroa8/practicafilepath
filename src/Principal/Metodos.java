package Principal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Metodos {

    java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir"));

    public void help()  {
        System.out.println("help -> Lista los comandos con una breve definición de lo que hacen. \n");

        System.out.println("cd -> Muestra el directorio actual");
        System.out.println("[..] -> Accede al directorio padre.");
        System.out.println("[<nombreDirectorio>] -> Accede a un directorio dentro del directorio actual.");
        System.out.println("[<rutaAbsoluta] -> Accede a la ruta absoluta del sistema. \n");

        System.out.println("mkdir <nombre_directorio> -> Crea un directorio en la ruta actual.\n");

        System.out.println("info <nombre> -> Muestra la información del elemento. \n");

        System.out.println(" cat <nombreFichero> -> Muestra el contenido de un fichero. \n");

        System.out.println("top <numeroLineas> <nombreFichero> -> Muestra las líneas especificadas de un fichero \n");

        System.out.println("mkfile <nombreFichero> <texto> -> Crea un fichero con ese nombre y el contenido de texto \n");

        System.out.println("write <nombreFichero> <texto> -> Añade 'texto' al final del fichero especificado. \n");

        System.out.println("dir -> Lista los archivos o directorios de la ruta actual.");
        System.out.println("[<nombreDirectorio>]-> Lista los archivos o directorios dentro de ese directorio.");
        System.out.println("[<rutaAbsoluta]-> Lista los archivos o directorios dentro de esa ruta \n");

        System.out.println("delete <nombre>-> Borra el fichero, si es un directorio borra todo su contenido y a si mismo. \n");

        System.out.println("close-> Cierra el programa. \n");

        System.out.println("Clear -> vacía la vista. \n");
    }

    
    public void cd(String[] vector, String cmd) {
        Path ruta2;
        if (vector.length == 1) {
            System.out.println(ruta);
        } else if (vector.length >= 2) {
            String cadena = cmd;
            cadena = cadena.substring(3);
            String segundo = vector[1];
            if (segundo.equals("..")) {
                ruta = ruta.getParent();
                System.out.println(ruta);
            } else {
                if (segundo.contains(":\\") || segundo.contains("/")) {
                    ruta = Paths.get(cadena);
                    System.out.println(ruta);
                } else {
                    ruta2 = ruta.resolve(cadena);
                    File directorio = new File(ruta2.toString());
                    if (directorio.exists()) {
                        ruta = ruta2;
                        System.out.println(ruta);
                    } else {
                        System.err.println("ERROR..El directorio " + cadena + " no existe!!");
                    }
                }
            }

        } else {
            System.err.println("ERROR.. formato incorrecto [ cd ] [cd ..] [cd nombreDirectorio ] [cd rutaAbsoluta] *Directorios con espacios no validos!");
        }

    }

    public void mkdir(String nombreDirectorio) { 
        
        Path ruta2;
        ruta2 = ruta.resolve(nombreDirectorio);
        File directorio = new File(ruta2.toString());
        if (nombreDirectorio.isEmpty()) {
            System.err.println("ERROR, formato incorrecto");
        } else if (directorio.exists()) {
            System.err.println("ERROR, el directorio ya existe..");

        } else {
            directorio.mkdir();
            System.out.println("Se ha creado el directorio llamado... " + nombreDirectorio);

        }
    }

    
    public void mkfile(String nombreFichero, String texto) throws IOException { 
        
        Path ruta2;
        ruta2 = ruta.resolve(nombreFichero);
        if (nombreFichero.isEmpty()) {
            System.err.println("ERROR..nombre del fichero vacio..");
        } else {
            try {
                FileWriter archivo = new FileWriter(ruta2.toString());
                BufferedWriter bw = new BufferedWriter(archivo);
                PrintWriter salidaTexto = new PrintWriter(bw);

                salidaTexto.print(texto);
                System.out.println("Se ha creado el fichero.." + nombreFichero + " con el texto.." + texto);
                System.out.println("En la ruta.." + ruta);
                salidaTexto.close();
                bw.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

   
    public void cat(String nombreFichero) throws FileNotFoundException, IOException {
      
        try {
            Path ruta2;
            ruta2 = ruta.resolve(nombreFichero);
            File fichero = new File(ruta2.toString());
            if (fichero.exists()) {
                FileReader fr = new FileReader(fichero);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                while ((linea = br.readLine()) != null) {
                    System.out.println(linea);
                }
                br.close();
                fr.close();
            } else {
                System.err.println("ERROR, el Fichero.. " + nombreFichero + " NO EXISTE");
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void info(String nombre) { 
        Path ruta2;
        ruta2 = ruta.resolve(nombre);
        File fichero = new File(ruta2.toString());
        if (fichero.exists() == true) {
            System.out.println("\t" + ruta2.getFileSystem());
            System.out.println("\t" + ruta2.getParent());
            System.out.println("\t" + ruta2.getRoot());
            System.out.println("\t" + ruta2.getNameCount());
            System.out.println("\t" + fichero.getFreeSpace());
            System.out.println("\t" + fichero.getTotalSpace());
            System.out.println("\t" + fichero.getUsableSpace());
        } else {
            System.out.println("ERROR, el fichero..." + nombre + "NO EXISTE");
        }
    }

    public void top(String nLineas, String nombreFichero) throws FileNotFoundException, IOException {
        
        Path ruta2;
        ruta2 = ruta.resolve(nombreFichero);
        if (nLineas.isEmpty() || nombreFichero.isEmpty()) {
            System.err.println("ERROR, el numeroLineas o nombreFichero esta vacio");
        } else {
            //Compruebo si el String nLineas..contiene solo números.
            boolean comprobar = false;
            for (int i = 0; i < nLineas.length(); i++) {
                if (Character.isDigit(nLineas.charAt(i))) {
                    comprobar = true;
                } else {
                    comprobar = false;
                }
            }
            if (comprobar == false) {
                
                System.err.println("ERROR.. el formato de nLineas no es valido [ INT: 1,2....]");
            } else {
                try {
                    File fichero = new File(ruta2.toString());
                    if (fichero.exists()) {
                        FileReader archivo = new FileReader(fichero);
                        BufferedReader br = new BufferedReader(archivo);
                        String linea;
                        int numeroLineas = Integer.parseInt(nLineas);
                        int cont = 0;
                        int num = 1;
                        while ((linea = br.readLine()) != null) {

                            if (cont != numeroLineas) {
                                System.out.print(num + " ");
                                System.out.println(linea);
                            } else {
                                break;
                            }
                            cont++;
                            num++;
                        }
                    } else {
                        System.err.println("ERROR.. el fichero no existe o falta el tipo de archivo [pdf...]");
                    }
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
        }
    }

    
    public void write(String nombreFichero, String texto) throws IOException { 
        
        try {
            Path ruta2;
            ruta2 = ruta.resolve(nombreFichero);
            File archivo = new File(ruta2.toString());
       

            if (archivo.exists()) {
                FileWriter fw = new FileWriter(archivo, true);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(texto);
                bw.close();
                System.out.println("Correcto!! se ha escrito en el fichero " + nombreFichero + " el texto.. " + texto);
            } else {
                System.err.println("ERROR..el fichero no existe");
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
    
    public void dir(String[] vector, String cmd) {
        java.nio.file.Path ruta2 = ruta;

        if (vector.length == 1) {
            System.out.println(ruta);
            File[] lista = new File(ruta.toString()).listFiles();
            for (int i = 0; i < lista.length; i++) {
                System.out.println("\t" + lista[i].getName());
            }
        } else if (vector.length >= 2) {
            String cadena = cmd;
            cadena = cadena.substring(4);
            String segundo = vector[1];
            if (segundo.contains(":\\") || segundo.contains("/")) {
                ruta2 = Paths.get(cadena);
                System.out.println(ruta);
                File[] lista = new File(ruta.toString()).listFiles();
                for (int i = 0; i < lista.length; i++) {
                    System.out.println("\t" + lista[i].getName());
                }
            } else {
                ruta2 = ruta.resolve(cadena);
                File directorio = new File(ruta2.toString());
                if (directorio.isDirectory() ) {
                    ruta2 = ruta.resolve(cadena);
                    File[] lista = new File(ruta2.toString()).listFiles();
                    for (int i = 0; i < lista.length; i++) {
                        System.out.println("\t" + lista[i].getName());
                    }
                } else {
                    System.err.println("ERROR..Directorio donde se intenta acceder no existe");
                }
            }

        }
    }

    public void close() { 
        System.out.println("Cerrando el programa..");
        System.exit(0);
    }

    public void clear() {
        System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }
    
    public void delete(String nombre) {
        Path ruta2;
        ruta2 = ruta.resolve(nombre);
        File fichero = new File(ruta2.toString());
        if (fichero.exists()) {
            fichero.delete();
            System.out.println("Archivo borrado.." + nombre);
        } else {
            System.err.println("ERROR, el fichero..." + nombre + " NO EXISTE");
        }
    }

}
